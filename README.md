# uni-echarts
在 uni-app 中使用[echarts-for-weixin](https://github.com/ecomfe/echarts-for-weixin)

## 平台支持
- mp-weixin 微信小程序

## 使用说明
将 components/uni-echarts 拷贝至目标项目，参考示例页面使用即可。

## 示例页面
- lazyload 延迟及修改数据示例
- multi 多个图表示例

## 参考
- [echarts-for-weixin](https://github.com/ecomfe/echarts-for-weixin)
- [echarts-for-wx-uniapp](https://github.com/Zhuyi731/echarts-for-wx-uniapp)
